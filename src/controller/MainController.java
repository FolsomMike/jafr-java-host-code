/******************************************************************************
* Title: MainController.java
* Author: Mike Schoonover
* Date: 11/15/12
*
* Purpose:
*
* This class is the Main Controller in a Model-View-Controller architecture.
* It creates the Model and the View.
* It tells the View to update its display of the data in the model.
* It handles user input from the View (button pushes, etc.)*
* It tells the Model what to do with its data based on these inputs and tells
*   the View when to update or change the way it is displaying the data.
*
* There may be many classes in the controller package which handle different
* aspects of the control functions.
*
* In this implementation:
*   the Model knows about the View
*   the View knows about the Controller
*   the Controller knows about the Model and the View and interacts with both
*
* The View sends messages to the Controller in the form of action messages
* to an EventHandler object -- in this case the Controller is designated to the
* View as the EventHandler.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

import robot.RobotLink;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import mksystems.mswing.MFloatSpinner;
import model.World;
import model.Options;
import view.MainView;
import view.ThreadSafeLogger;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class MainController
//

public class MainController implements EventHandler, Runnable
{

    static final private String ROBOT_IP_ADDR = "192.168.10.110";

    private World world;

    RobotLink robotLink;

    private MainView mainView;

    private Options options;

    private final Boolean blinkStatusLabel = false;

    private String errorMessage;

    private SwingWorker workerThread;

    private final DecimalFormat decimalFormat1 = new DecimalFormat("#.0");

    int heartBeat = 0;

    private Font tSafeFont;
    private String tSafeText;

    private int displayUpdateTimer = 0;
    private int guiUpdateTimer = 0;

    private int headSweepTimer;
    private boolean fullHeadSweeping;
    private int fullHeadSweepStartAngle;
    private int headAngle;
    private boolean headSweepingLeft = true;
    int returnToStartCount = -1;

    private String XMLPageFromRemote;

    private boolean shutDown = false;

    private final JFileChooser fileChooser = new JFileChooser();

    private final String newline = "\n";

    private static final int GUI_UPDATE_RATE = 10;

    ThreadSafeLogger tsLog;


    static final int MIN_HEAD_ANGLE = 0;
    static final int MAX_HEAD_ANGLE = 180;

    //bits in the Robot Status Display Select byte

    static final int CHECKSUM_ERROR_CNT = 0x01;
    static final int PACKET_RCVD_CNT = 0x02;

//-----------------------------------------------------------------------------
// MainController::MainController (constructor)
//

public MainController()
{

}//end of MainController::MainController (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{

    mainView = new MainView(this);
    mainView.init();

    tsLog = mainView.createLogger();
    tsLog.appendLine("Hello"); tsLog.appendLine("");

    world = new World(mainView);
    world.init();

    robotLink = new RobotLink();
    robotLink.init(tsLog);
    robotLink.connect(ROBOT_IP_ADDR);

    initRobot();

    //create and load the program options
    options = new Options();

    //start the control thread
    new Thread(this).start();

    mainView.setupAndStartMainTimer();

}// end of MainController::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::initRobot
//

private void initRobot(){

    robotLink.sendInitCommands();

    robotLink.setHeadAngle((byte)mainView.getValueOfHeadAngleSlider());

    robotLink.setLEDDigitDisplayDirect(1,9,6,5);

    waitSleep(3000);

    robotLink.setLEDDigitDisplay(2017);

}// end of MainController::initRobot
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::actionPerformed
//
// Responds to events.
//
// This is identical to the method employed by  ActionListener objects. This
// object is not an ActionListener, but uses the same concept for clarity. The
// "MainView" (MVC Concept) objects catch GUI events and call this method to pass
// those events to this "MainController" object.
//

@Override
public void actionPerformed(ActionEvent e)
{

    if ("Draw".equals(e.getActionCommand())){
        draw(); return;
    }

    if ("Timer".equals(e.getActionCommand())) {doTimerActions(); return;}

    if ("Display Log".equals(e.getActionCommand())) {displayLog(); return;}

    if ("Display Help".equals(e.getActionCommand())) {displayHelp(); return;}

    if ("Display About".equals(e.getActionCommand())) {displayAbout(); return;}

    if ("New File".equals(e.getActionCommand())) {doSomething1(); return;}

    if (e.getActionCommand().startsWith("Display as Status")) {
        handleRobotStatusDisplayMenuSelection(e.getActionCommand());
        return;
    }

    if ("Open File".equals(e.getActionCommand())) {
        doSomething2(); return;
    }

    if ("Load Data From File".equals(e.getActionCommand())){
        loadDataFromFile(); return;
    }

    if ("Save Data To File".equals(e.getActionCommand())){
        saveDataToFile(); return;
    }

    if ("One Sweep".equals(e.getActionCommand())){
        startFullHeadSweep(2);
    }

    if ("Repeat Sweep".equals(e.getActionCommand())){
        startFullHeadSweep(-1);
    }

    if ("Stop Sweep".equals(e.getActionCommand())){
        stopHeadSweep();
    }

    if ("Move Up Large".equals(e.getActionCommand())){
        robotLink.sendCommand(RobotLink.IR_BUTTON_UP);
    }

    if ("Move Down Large".equals(e.getActionCommand())){
        robotLink.sendCommand(RobotLink.IR_BUTTON_DOWN);
    }

    if ("Move Left Large".equals(e.getActionCommand())){
        robotLink.sendCommand(RobotLink.IR_BUTTON_LEFT);
    }

    if ("Move Right Large".equals(e.getActionCommand())){
        robotLink.sendCommand(RobotLink.IR_BUTTON_RIGHT);
    }

}//end of MainController::actionPerformed
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//MainController::handleRobotStatusDisplayMenuSelection
//
// Handles Robot status display on LED Digit display menu option selections.
//

private void handleRobotStatusDisplayMenuSelection(String pMenuOption){

    byte statusDisplayFlags = 0;

    switch(pMenuOption){

        case "Display as Status Checksum Error Count on Robot" :
            statusDisplayFlags |= CHECKSUM_ERROR_CNT;
        break;

        case "Display as Status Packets Received Count on Robot" :
            statusDisplayFlags |= PACKET_RCVD_CNT;
        break;

    }

    robotLink.setDisplayStatusFlags(statusDisplayFlags);

}//end of MainController::handleRobotStatusDisplayMenuSelection
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//MainController::startFullHeadSweep
//
// Starts the head sweeping back and forth from max to min angle.
//
// See function doFullHeadSweep for info on pReturnToStartCount.
//

private void startFullHeadSweep(int pReturnToStartCount){

    fullHeadSweepStartAngle = mainView.getValueOfHeadAngleSlider();
    headAngle = fullHeadSweepStartAngle;
    headSweepTimer = 0;
    fullHeadSweeping = true;
    returnToStartCount = pReturnToStartCount;

}//end of MainController::startFullHeadSweep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//MainController::stopHeadSweep
//
// Stops the head sweeping action. Sets the slider control to match the last
// angle of the head.
//

private void stopHeadSweep(){

    mainView.setValueOfHeadAngleSlider(headAngle);

    fullHeadSweeping = false;

}//end of MainController::stopHeadSweep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::stateChanged
//

@Override
public void stateChanged(ChangeEvent ce)

{

    //if for some reason the object which changed state is not a subclass of
    //of Component, do nothing as this code only handles Components

    if (!(ce.getSource() instanceof Component)) {
        return;
    }

    //cast the object to a Component so it's methods can be accessed
    Component c = (Component)ce.getSource();

    String name = c.getName();

    if (name.startsWith("Double Spinner 1")){

        //Since we know that the Component with the name starting with
        //"Double Spinner 1" is an MFloatSpinner (because we created it and
        // used that name for it), it can safely be cast to an MFloatSpinner.
        //Since the values in that spinner are meant to be doubles, the
        //getDoubleValue method is used to retrieve the value.

        double value = ((MFloatSpinner)c).getDoubleValue();

        mainView.setTextForDataTArea1("" + value);

        //using getDoubleValue as above will often return a value with a long
        //fractional portion due to binary floating point conversion
        //imprecision -- using getText returns the value as a string formatted
        //exactly as that shown in the spinner's text box and will be rounded
        //off and truncated in the same manner

        String textValue = ((MFloatSpinner)c).getText();

        mainView.setTextForDataTArea2(textValue);

    }

    if (name.startsWith("Integer Spinner 1")){

        //Since we know that the Component with the name starting with
        //"Integer Spinner 1" is an MFloatSpinner (because we created it and
        // used that name for it), it can safely be cast to an MFloatSpinner.
        //Since the values in that spinner are meant to be integers, the
        //getIntValue method is used to retrieve the value.

        int value = ((MFloatSpinner)c).getIntValue();

        mainView.setTextForDataTArea2("" + value);

    }

    if (name.startsWith("Head Angle Slider")){
        headAngle = ((JSlider)c).getValue();
        robotLink.setHeadAngle((byte)headAngle);
        mainView.setBotUTSweepLineAngle(0, headAngle);
    }

}//end of MainController::stateChanged
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::draw
//
// Draws all objects in the universe.
//

public void draw()
{

    //debug mks -- remove this function?

}//end of MainController::draw
//-----------------------------------------------------------------------------

/*
//-----------------------------------------------------------------------------
// MainController::paintComponent
//

@Override
public void paintComponent (Graphics g)
{

}// end of MainController::paintComponent
//-----------------------------------------------------------------------------

*/

//-----------------------------------------------------------------------------
// MainController::loadDataFromFile
//
// Loads data from a file.
//

public void loadDataFromFile()
{

    world.loadFromTextFile();

    mainView.updateGUIDataSet1();

    mainView.drawRectangle();

}//end of MainController::loadDataFromFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::saveDataToFile
//
// Saves data to a file.
//

public void saveDataToFile()
{

    mainView.updateModelDataSet1();

    world.saveToTextFile();

}//end of MainController::saveDataToFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::doTimerActions
//
// Performs actions driven by the timer.
//
// Not used for accessing network -- see run function for details.
//

public void doTimerActions()
{

    updateUniverseDisplay();

    updateGUI();

}//end of MainController::doTimerActions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::updateUniverseDisplay
//
// Triggers moves for all Sprites and updates the display.
//

public void updateUniverseDisplay()
{

    world.moveSprites();

    mainView.updateUniverseDisplay();

}//end of MainController::updateUniverseDisplay
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::updateGUI
//
// Updates various GUI items such as status messages and labels.
//

private void updateGUI()
{

    if(guiUpdateTimer-- <= 0){
        guiUpdateTimer = GUI_UPDATE_RATE;
        mainView.updateGUI();
    }


}//end of MainController::updateGUI
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::displayLog
//
// Displays the log window. It is not released after closing as the information
// is retained so it can be viewed the next time the window is opened.
//

private void displayLog()
{

    mainView.displayLog();

}//end of MainController::displayLog
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::displayHelp
//
// Displays help information.
//

private void displayHelp()
{

    mainView.displayHelp();

}//end of MainController::displayHelp
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::displayAbout
//
// Displays about information.
//

private void displayAbout()
{

    mainView.displayAbout();

}//end of MainController::displayAbout
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::doSomething1
//

private void doSomething1()
{


}//end of MainController::doSomething1
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::doSomethingInWorkerThread
//
// Does nothing right now -- modify it to call a function which takes a long
// time to finish. It will be run in a background thread so the GUI is still
// responsive.
// -- CHANGE THE NAME TO REFLECT THE ACTION BEING DONE --
//

private void doSomethingInWorkerThread()
{

    //define and instantiate a worker thread to create the file


    //----------------------------------------------------------------------
    //class SwingWorker
    //

    workerThread = new SwingWorker<Void, String>() {
        @Override
        public Void doInBackground() {

            //do the work here by calling a function

            return(null);

        }//end of doInBackground

        @Override
        public void done() {

            //clear in progress message here if one is being displayed

            try {

                //use get(); function here to retrieve results if necessary
                //note that Void type here and above would be replaced with
                //the type of variable to be returned

                Void v = get();

            } catch (InterruptedException ignore) {}
            catch (java.util.concurrent.ExecutionException e) {
                String why;
                Throwable cause = e.getCause();
                if (cause != null) {
                    why = cause.getMessage();
                } else {
                    why = e.getMessage();
                }
                System.err.println("Error creating file: " + why);
            }//catch

        }//end of done

        @Override
        protected void process(java.util.List <String> pairs) {

            //this method is not used by this application as it is limited
            //the publish method cannot be easily called outside the class, so
            //messages are displayed using a ThreadSafeLogger object and status
            //components are updated using a GUIUpdater object

        }//end of process

    };//end of class SwingWorker
    //----------------------------------------------------------------------

}//end of MainController::doSomethingInWorkerThread
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::doSomething2
//

private void doSomething2()
{


}//end of MainController::doSomething2
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::run
//
// This is the part which runs as a separate thread.  The actions of accessing
// remote devices occur here.  If they are done in a timer call instead, then
// buttons and displays get frozen during the sometimes lengthy calls to access
// the network.
//
// NOTE:  All functions called by this thread must wrap calls to alter GUI
// components in the invokeLater function to be thread safe.
//

@Override
public void run()
{

    //call the control method repeatedly
    while(true){

        control();

        //sleep for 100 milliseconds -- all timing is based on this period
        threadSleep(100);

    }

}//end of MainController::run
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::threadSleep
//
// Calls the Thread.sleep function. Placed in a function to avoid the
// "Thread.sleep called in a loop" warning -- yeah, it's cheezy.
//

public void threadSleep(int pSleepTime)
{

    try {Thread.sleep(pSleepTime);} catch (InterruptedException e) { }

}//end of MainController::threadSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::control
//
// Performs all display and control.  Call this from a thread.
//

public void control()
{

    //update the display every 30 seconds
    if (displayUpdateTimer++ == 14){
        displayUpdateTimer = 0;
        //call function to update stuff here
    }

    //robotLink.setLEDDigitDisplay(heartBeat++);
    //if (heartBeat > 9999) { heartBeat = 9999; }

    if(fullHeadSweeping){ doFullHeadSweep(); }

    //If a shut down is initiated, clean up and exit the program.

    if(shutDown){
        //exit the program
        System.exit(0);
    }

}//end of MainController::control
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::doFullHeadSweep
//
// Performs a full sweep of the head from far left to far right and back to
// the starting point.
//
// If returnToStartCount is -1, the head will sweep back and forth forever.
//
// If returnToStartCount is a positive value, the head will sweep until the
// starting angle is passed the number of times specified by that positive
// value. To perform one full sweep and stop at the original angle, set
// returnToStartCount to 2 before starting the sweep.
//

private void doFullHeadSweep()
{

    if (headSweepTimer++ < 1){ return; }

    headSweepTimer = 0;

    if(headSweepingLeft){

        headAngle++;

        if (headAngle > MAX_HEAD_ANGLE) {
            headAngle = MAX_HEAD_ANGLE;
            headSweepingLeft = false;
        }
    }

    if(!headSweepingLeft){

        headAngle--;

        if (headAngle < MIN_HEAD_ANGLE) {
            headAngle = MIN_HEAD_ANGLE;
            headSweepingLeft = true;
        }
    }

    robotLink.setHeadAngle((byte)headAngle);

    javax.swing.SwingUtilities.invokeLater(() -> {
        mainView.setValueOfHeadAngleSlider(headAngle);
    });

    javax.swing.SwingUtilities.invokeLater(() -> {
        mainView.setBotUTSweepLineAngle(0, headAngle);
    });

    if(returnToStartCount != -1
                    && headAngle == fullHeadSweepStartAngle){
        if(--returnToStartCount== 0){ fullHeadSweeping = false; }
    }

}//end of MainController::doFullHeadSweep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::displayErrorMessage
//
// Displays an error dialog with message pMessage.
//

public void displayErrorMessage(String pMessage)
{

    mainView.displayErrorMessage(pMessage);

}//end of MainController::displayErrorMessage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::waitSleep
//
// Sleeps for pTime milliseconds.
//

public void waitSleep(int pTime)
{

    try {Thread.sleep(pTime);} catch (InterruptedException e) { }

}//end of MainController::waitSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::shutDown
//
// Disables chassis power and performs any other appropriate shut down
// operations.
//
// This is done by setting a flag so that this class's thread can do the
// actual work, thus avoiding thread contention.
//

public void shutDown()
{

    shutDown = true;

}//end of MainController::shutDown
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::windowClosing
//
// Handles actions necessary when the window is closing
//

@Override
public void windowClosing(WindowEvent e)
{

    //perform all shut down procedures

    shutDown();

}//end of MainController::windowClosing
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::(various window listener functions)
//
// These functions are implemented per requirements of interface WindowListener
// but do nothing at the present time.  As code is added to each function, it
// should be moved from this section and formatted properly.
//

@Override
public void windowActivated(WindowEvent e){}
@Override
public void windowDeactivated(WindowEvent e){}
@Override
public void windowOpened(WindowEvent e){}
//@Override
//public void windowClosing(WindowEvent e){}
@Override
public void windowClosed(WindowEvent e){}
@Override
public void windowIconified(WindowEvent e){}
@Override
public void windowDeiconified(WindowEvent e){}

//end of MainController::(various window listener functions)
//-----------------------------------------------------------------------------


}//end of class MainController
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
