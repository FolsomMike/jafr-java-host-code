/******************************************************************************
* Title: Sprite.java - the parent class for all screen objects
* Author: Mike Schoonover
* Date: 4/23/16
*
* Purpose:
*
* This class is the parent class for all things which exist on the Universe
* canvas.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

package model;

//-----------------------------------------------------------------------------

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

//-----------------------------------------------------------------------------
// class Sprite
//

public class Sprite extends Object{
  
    int idNum;
    int maxX, maxY;
    int maxDistance;
    int x, y, centerX, centerY;
    int size;
    int width, height;
    int distance;
    int moveX, moveY;
    int speed;
    int speedCtrl;

    boolean alive;

    int collisionSpriteIDNum, collisionSpriteX, collisionSpriteY;
    
    Color color;
    
    Ellipse2D ellipse;
    Rectangle2D rectangle;

    int tagTimer;
    
    private static final int MIN_SPEED = 10; //bigger value is slower
    private static final int TAG_TIME = 60;
    
    public int getIDNum(){ return(idNum); }
    public boolean isAlive(){ return(alive); }
    public int getX(){ return(x); }
    public int getY(){ return(y); }
    public int getCenterX(){ return(centerX); }
    public int getCenterY(){ return(centerY); }
    public int getWidth(){ return(width); }
    public int getHeight(){ return(height); }
    public int getSpeed(){ return(speed); }
    public int getSize(){ return(size); }
    public Rectangle2D getRectangle(){ return(rectangle); }

    public void setCollisionSpriteX(int pX){ collisionSpriteX = pX; }
    public void setCollisionSpriteY(int pY){ collisionSpriteY = pY; }
    public void setCollisionSpriteIDNum(int pID){ collisionSpriteIDNum = pID; }
    public void setTagTimer(int pTime) { tagTimer = pTime; }
    
//-----------------------------------------------------------------------------
// Sprite::Sprite (constructor)
//

public Sprite(int pIDNum, int pMaxX, int pMaxY)
{

    idNum = pIDNum; maxX = pMaxX; maxY = pMaxY;
        
}//end of Sprite::Sprite (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{

    alive = true;
    
    collisionSpriteIDNum = -1; tagTimer = 0;
    collisionSpriteX = -1; collisionSpriteY = -1;
    
    maxDistance = maxX > maxY ? maxX : maxY;
    
    distance = 0;
    
    size = 1; width = size; height = size;
    
    setXY((int)(Math.random() * maxX), (int)(Math.random() * maxY));
    
    color = new Color((int)(Math.random() * 255),
                      (int)(Math.random() * 255),
                      (int)(Math.random() * 255),
                      255);
    
    color = Color.BLACK; //debug mks
    
    updateShape();
    
}//end of Sprite::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::updateShape
//
// Updates the shape and the bounding rectangle to reflect new x,y,width,height.
//

private void updateShape()
{
    
    ellipse = new Ellipse2D.Double(x, y, width, height);
    rectangle = new Rectangle2D.Double(x, y, width, height);
    
}//end of Sprite::updateShape
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::draw
//
// Draws the sprite.
//

public void draw(Graphics2D pG2)
{

    if (!alive){ return; }
 
    tagCollisionSprite(pG2);
    
    pG2.setColor(color);    
    pG2.draw(ellipse);
    
    //draw dot in center of larger Sprites
    if(size >= 5){
        pG2.setColor(Color.RED);
        pG2.drawLine(centerX, centerY, centerX, centerY);
    }

}//end of Sprite::draw
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::tagCollisionSprite
//
// If a collision occurred on the last move, a graphic tagging is performed to
// depict the killing of the Sprite.
//

private void tagCollisionSprite(Graphics2D pG2)
{
        
    if(collisionSpriteIDNum == -1) { return; }
  
    pG2.setColor(Color.RED);
        
    pG2.drawLine(centerX, centerY, collisionSpriteX, collisionSpriteY);

    //draw tag until timer runs out so it can easily be seen
    if(tagTimer-- <= 0){
        tagTimer = 0;
        collisionSpriteIDNum = -1;
        collisionSpriteX = -1; collisionSpriteY = -1;
    }
        
}//end of Sprite::tagCollisionSprite
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::move
//
// Moves the Sprite.
//
// A distance to move and a direction are determined. The Sprite moves in that
// direction until the distance or the edge of the universe are reached, then
// a new direction and distance are calculated.
//

public void move()
{
    
    if (!alive){ return; }
    
    if (distance == 0){
        distance = (int)(Math.random() * maxDistance);
        moveX = (int)(Math.random() * 3) - 1;
        moveY = (int)(Math.random() * 3) - 1;
        speed = (int)(Math.random() * MIN_SPEED) + 1;
        speedCtrl = 1;
        if (distance == 0) {return;} //don't move this time
    }
    
    //skip moves to control speed based on the speed value
    
    speedCtrl--;
    if(speedCtrl == 0){
        speedCtrl = speed;
    }else{
        return;
    }
    
    distance--;
    
    int newCenterX = centerX + moveX; int newCenterY = centerY + moveY;
    
    // handle when sprite moves off edge of screen
    
    if(newCenterX < 0){ newCenterX = 0; moveX *= -1;  }
    if(newCenterX > maxX){ newCenterX = maxX; moveX *= -1; }
    if(newCenterY < 0){ newCenterY = 0; moveY *= -1;}
    if(newCenterY > maxY){ newCenterY = maxY; moveY *= -1; }    
    
    setCenter(newCenterX, newCenterY);
    
    updateShape();
    
}//end of Sprite::move
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::checkForCollisionUsingRectangles
//
// Checks to see if this sprite has collided with another sprite in the
// pSprites array. This version uses the bounding rectangle of each sprite
// to determine collision.
//
// The center point of the offending Sprite is saved so a graphic of some sort
// can be performed regarding the Sprite on the next draw cycle. The center
// of this Sprite is also stored in the offending Sprite so it can do the
// tagging in case this Sprite is the victim and dies.
//
// Returns idNum of other sprite if collision has occurred, -1 otherwise.
//

public int checkForCollisionUsingRectangles(Sprite[] pSprites)
{

    for (Sprite sprite : pSprites) { 
    
        if (sprite.isAlive() && 
             rectangle.intersects(sprite.getRectangle())
             && sprite.getIDNum() != idNum){
            
            collisionSpriteIDNum = sprite.getIDNum();
            collisionSpriteX = sprite.getCenterX();
            collisionSpriteY = sprite.getCenterY();
            tagTimer = TAG_TIME;
            sprite.setCollisionSpriteIDNum(idNum);
            sprite.setCollisionSpriteX(centerX);
            sprite.setCollisionSpriteY(centerY);
            sprite.setTagTimer(TAG_TIME);
            
            return(sprite.getIDNum());        
        }
    }
        
    return(-1);

}//end of Sprite::checkForCollisionUsingRectangles
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::grow
//
// Causes the sprite to grow in width and height by pGrowth. 
//

public void grow(int pGrowth)
{
    
    size += pGrowth; width = size; height = size;
    
    updateShape();
        
}//end of Sprite::grow
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::kill
//
// Kills the sprite. It will no longer be displayed and its x,y will be set to
// -10,-10 with size of 1 (sent to purgatory).
//

public void kill()
{

    alive = false; 
    
    size = 1; width = size; height = size;
    
    setXY(-10,-10);
    
    updateShape();
    
}//end of Sprite::kill
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::setXY
//
// Sets the x,y location of the Sprite to pX,pY and updates related values.
//

private void setXY(int pX, int pY)
{

    x = pX; y = pY;
    
    calculateCenter();
    
}//end of Sprite::setXY
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::setCenter
//
// Sets the center location of the Sprite to pX,pY and updates related values.
//

private void setCenter(int pX, int pY)
{

    centerX = pX; centerY = pY;
    
    calculateXYFromCenter();
    
}//end of Sprite::setXY
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::calculateCenter
//
// Calculates the center point of the Sprite from the x,y position.
//

private void calculateCenter()
{

    centerX = x + size/2; centerY = y + size/2;
    
}//end of Sprite::calculateCenter
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Sprite::calculateXYFromCenter
//
// Calculates the x,y position of the Sprite from its center point.
//

private void calculateXYFromCenter()
{

    x = centerX - size/2; y = centerY - size/2;
    
}//end of Sprite::calculateXYFromCenter
//-----------------------------------------------------------------------------

}//end of class Sprite
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
