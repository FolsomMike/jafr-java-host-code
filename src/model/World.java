/******************************************************************************
* Title: World.java - Data for the world!
* Author: Mike Schoonover
* Date: 10/31/15
*
* Purpose:
*
* This class handles data related to the world: loads, saves, etc.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------

import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import view.MainView;

//-----------------------------------------------------------------------------
// class World

public class World extends Object{

    MainView mainView;
    
    private String dataVersion = "1.0";

    int maxX, maxY;
    
    private Sprite [] sprites;

    int numSpritesAlive, largestSpriteSize;
    
    static final int DATA_SIZE = 1000;

    private FileInputStream fileInputStream = null;
    private InputStreamReader inputStreamReader = null;
    private BufferedReader in = null;

    private FileOutputStream fileOutputStream = null;
    private OutputStreamWriter outputStreamWriter = null;
    private BufferedWriter out = null;
    
    public int getNumSpritesAlive(){ return(numSpritesAlive); }
    public int getLargestSpriteSize(){ return(largestSpriteSize); }
    
//-----------------------------------------------------------------------------
// World::World (constructor)
//

public World(MainView pMainView)
{
    
    mainView = pMainView;

    maxX = mainView.getUniverseMaxX();
    maxY = mainView.getUniverseMaxY();
    
}//end of World::World (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{
    
    setUpUniverse();

}//end of World::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::setUpUniverse
//
// Sets up the universe and all its inhabitants.
//

public void setUpUniverse()
{

    //allocate the Sprite array and fill it

    sprites = new Sprite[DATA_SIZE];

    for (int i=0; i<DATA_SIZE; i++){
        sprites[i] = new Sprite(i, maxX, maxY);
        sprites[i].init();
    }
    
    numSpritesAlive = DATA_SIZE;
    
    largestSpriteSize = 0;

}//end of World::setUpUniverse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::draw
//
// Draws all the objects in the universe.
//

public void draw(Graphics2D pG2)
{
    
    for (Sprite sprite : sprites) { sprite.draw(pG2); }
    
}//end of World::draw
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::moveSprites
//
// Triggers moves for all Sprites.
//

public void moveSprites()
{
    
    numSpritesAlive = 0;
    largestSpriteSize = 0;
    
    for (Sprite sprite : sprites) { 
        
        if (!sprite.isAlive()) { continue; }

        numSpritesAlive++;
        
        if (sprite.getSize() > largestSpriteSize){
            largestSpriteSize = sprite.getSize();
        }
        
        sprite.move();
     
        int collidedSprite = sprite.checkForCollisionUsingRectangles(sprites);
        
        if(collidedSprite != -1){
            handleSpriteCollision(sprite, sprites[collidedSprite]);
        }
    }
    
}//end of World::moveSprites
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::handleSpriteCollision
//
// Handles collision between two sprites pSprite1 and pSprite2.
//
// The bigger sprite eats the smaller sprite. If same size, faster sprite is
// victor. If same size and speed, second sprite is victor.
//
// The victor sprite grows by 1 while the victim sprite is killed.
//

public void handleSpriteCollision(Sprite pSprite1, Sprite pSprite2)
{
    
    Sprite victor, victim;
    
    if(pSprite1.getSize() != pSprite2.getSize() ){
        victor = pSprite1.getSize() > pSprite2.getSize() ? pSprite1 : pSprite2;
    }else{
        victor = 
                pSprite1.getSpeed() > pSprite2.getSpeed() ? pSprite1 : pSprite2;
    }
    
    if (victor == pSprite1){victim = pSprite2;} else {victim = pSprite1;}
    
    victor.grow(victim.getSize());     //victor gets bigger
    victim.kill();      //victim dies
    
}//end of World::handleSpriteCollision
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::getDataVersion
//
// Returns dataVersion.
//

public String getDataVersion()
{

    return dataVersion;

}//end of World::getDataVersion
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::getDataItem
//
// Returns item indexed by pIndex from array data.
//

public String getDataItem(int pIndex)
{

//debug mks    return data[pIndex];

    return(""); //debug mks
    
}//end of World::getDataItem
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::setDataVersion
//
// Sets dataVersion to pValue.
//

public void setDataVersion(String pValue)
{

    dataVersion = pValue;

}//end of World::setDataVersion
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::setDataItem
//
// Sets data array element indexed by pIndex to pValue.
//

public void setDataItem(int pIndex, String pValue)
{

//debug mks    data[pIndex] = pValue;

}//end of World::setDataItem
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::loadFromTextFile
//
// Loads all data from a text file.
//

public void loadFromTextFile()
{

    try{

        openTextInFile("Sample Data.txt");

        readDataFromTextFile();

    }
    catch (IOException e){

        //display an error message and/or log the message

    }
    finally{

        closeTextInFile();

    }

}//end of World::loadFromTextFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::openTextInFile
//
// Opens text file pFilename for reading.
//

private void openTextInFile(String pFilename) throws IOException
{

    fileInputStream = new FileInputStream(pFilename);
    inputStreamReader = new InputStreamReader(fileInputStream);
    in = new BufferedReader(inputStreamReader);

}//end of World::openTextInFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::readDataFromTextFile
//
// Reads the data from the text file.
//

private void readDataFromTextFile() throws IOException
{

    //read each data line or until end of file reached

    String line;

    if ((line = in.readLine()) != null){

        dataVersion = line;

    }

    for (int i=0; i<DATA_SIZE; i++){

        if ((line = in.readLine()) == null) { break; }

//debug mks        data[i] = line;

    }

}//end of World::readDataFromTextFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::closeTextInFile
//
// Closes the text input file.
//

private void closeTextInFile()
{

    try{

        if (in != null) {in.close();}
        if (inputStreamReader != null) {inputStreamReader.close();}
        if (fileInputStream != null) {fileInputStream.close();}

    }
    catch(IOException e){

        //ignore error while trying to close the file
        //could log the error message in the future

    }

}//end of World::closeTextInFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::saveToTextFile
//
// Saves all data to a text file.
//

public void saveToTextFile()
{

    try{

        openTextOutFile("Sample Data.txt");

        saveDataToTextFile();

    }
    catch (IOException e){

        //display an error message and/or log the message

    }
    finally{

        closeTextOutFile();

    }

}//end of World::saveToTextFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::openTextOutFile
//
// Opens text file pFilename for writing.
//

private void openTextOutFile(String pFilename) throws IOException
{

    fileOutputStream = new FileOutputStream(pFilename);
    outputStreamWriter = new OutputStreamWriter(fileOutputStream);
    out = new BufferedWriter(outputStreamWriter);

}//end of World::openTextOutFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::saveDataToTextFile
//
// Saves the data to the text file.
//

private void saveDataToTextFile() throws IOException
{

    //write each data line

    String line;

    out.write(dataVersion);
    out.newLine();

    for (int i=0; i<DATA_SIZE; i++){

        //debug mks out.write(data[i]);
        out.newLine();

    }

}//end of World::readDataFromTextFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// World::closeTextOutFile
//
// Closes the text output file.
//

private void closeTextOutFile()
{

    try{

        if (out != null) {out.close();}
        if (outputStreamWriter != null) {outputStreamWriter.close();}
        if (fileOutputStream != null) {fileOutputStream.close();}

    }
    catch(IOException e){

        //ignore error while trying to close the file
        //could log the error message in the future

    }

}//end of World::closeTextOutFile
//-----------------------------------------------------------------------------


}//end of World
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
