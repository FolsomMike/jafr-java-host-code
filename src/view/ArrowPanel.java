/******************************************************************************
* Title: ArrowPanel.java
* Author: Mike Schoonover
* Date: 11/18/2017
*
* Purpose:
*
* This class creates a panel with various arrow buttons for direction or
* position control.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class ArrowPanel
//
// Provides a panel with arrow buttons for contolling the XY positioner.
//

class ArrowPanel extends JPanel{

//buttons for moving to home and center positions, etc.
JButton homeButton, centerButton;
MJButton rotateHotButtons;

//buttons for three size of moves for each direction
JButton uuu, uu, u;
JButton ddd, dd, d;
JButton lll, ll, l;
JButton rrr, rr, r;

String smallTip, mediumTip, largeTip;

ImageIcon redUpLargeArrow, redUpMediumArrow, redUpSmallArrow;
ImageIcon redLeftLargeArrow, redLeftMediumArrow, redLeftSmallArrow;
ImageIcon redRightLargeArrow, redRightMediumArrow, redRightSmallArrow;
ImageIcon redDownLargeArrow, redDownMediumArrow, redDownSmallArrow;

ImageIcon greenUpLargeArrow, greenUpMediumArrow, greenUpSmallArrow;
ImageIcon greenLeftLargeArrow, greenLeftMediumArrow, greenLeftSmallArrow;
ImageIcon greenRightLargeArrow, greenRightMediumArrow, greenRightSmallArrow;
ImageIcon greenDownLargeArrow, greenDownMediumArrow, greenDownSmallArrow;

int currentHotButtons = -1;

//-----------------------------------------------------------------------------
// ArrowPanel::ArrowPanel (constructor)
//
//
// pActionListener is the panel which will handle the button actions.
//
// smallStep, mediumStep, largeStep are the distances the positioner is to
// be moved by clicking on the small, medium, and large arrow buttons.
//

public ArrowPanel(ActionListener pActionListener, double smallStep,
                                           double mediumStep, double largeStep)
{

//set up the main panel - this panel does nothing more than provide a title
//border and a spacing border
setOpaque(true);

setAlignmentX(Component.LEFT_ALIGNMENT);

//change the layout manager
setLayout(new GridLayout(3, 3, 0, 0));

//setMinimumSize(new Dimension(230,150));
//setPreferredSize(new Dimension(230,150));
//setMaximumSize(new Dimension(230,150));

homeButton = new MJButton("H", KeyEvent.VK_UNDEFINED,
        "Moves the transducer to the Home position.");
homeButton.setActionCommand("Move to Home");
homeButton.addActionListener(pActionListener);
centerButton = new MJButton("C", KeyEvent.VK_UNDEFINED,
        "Moves the transducer to the Center position.");
centerButton.setActionCommand("Move to Center");
centerButton.addActionListener(pActionListener);

//create tool tips for each of the different size buttons showing the user
//how much each button will move the positioner
DecimalFormat twoPlaces = new DecimalFormat("0.00");
smallTip = ("move " + twoPlaces.format(smallStep));
mediumTip = ("move " + twoPlaces.format(mediumStep));
largeTip = ("move " + twoPlaces.format(largeStep));

//NOTE: You must use forward slashes in the path names for the resource
//loader to find the image files in the JAR package.

redUpLargeArrow = createImageIcon("images/RedUpLargeArrow.gif");
greenUpLargeArrow = createImageIcon("images/GreenUpLargeArrow.gif");
uuu = new MJButton("", redUpLargeArrow, KeyEvent.VK_UP, largeTip);
uuu.setActionCommand("Move Up Large");
uuu.addActionListener(pActionListener);

redLeftLargeArrow = createImageIcon("images/RedLeftLargeArrow.gif");
greenLeftLargeArrow = createImageIcon("images/GreenLeftLargeArrow.gif");
lll = new MJButton("", redLeftLargeArrow, KeyEvent.VK_LEFT, largeTip);
lll.setActionCommand("Move Left Large");
lll.addActionListener(pActionListener);

redRightLargeArrow = createImageIcon("images/RedRightLargeArrow.gif");
greenRightLargeArrow = createImageIcon("images/GreenRightLargeArrow.gif");
rrr = new MJButton("", redRightLargeArrow, KeyEvent.VK_RIGHT, largeTip);
rrr.setActionCommand("Move Right Large");
rrr.addActionListener(pActionListener);

redDownLargeArrow = createImageIcon("images/RedDownLargeArrow.gif");
greenDownLargeArrow = createImageIcon("images/GreenDownLargeArrow.gif");
ddd = new MJButton("", redDownLargeArrow, KeyEvent.VK_DOWN, largeTip);
ddd.setActionCommand("Move Down Large");
ddd.addActionListener(pActionListener);

rotateHotButtons =
        new MJButton("X", KeyEvent.VK_SLASH, "Changes hot button group (alt-/)");
rotateHotButtons.setActionCommand("Rotate Hot Buttons");
rotateHotButtons.addActionListener(pActionListener);

//add buttons, using blank labels to fill blank grid spot(s) so buttons are
//positioned as desired

/*add(new JLabel("1"));*/
add(homeButton);

add(uuu);

add(centerButton);

add(lll);

add(new JLabel(""));

add(rrr);

add(new JLabel(""));
add(ddd);

add(rotateHotButtons);

}//end of ArrowPanel::ArrowPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ArrowPanel::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{

}// end of ArrowPanel::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ArrowPanel::setHotButtons
//
// When the user clicks a large arrow button, all the large arrow buttons
// become the "hot" set.  The hot set can be controlled by the arrow keys
// (alt-up arrow, etc.) for quick maneuvering.  When the user clicks on a
// different sized arrow key, that size becomes the hot set.
//
// The hot buttons get green arrows, cold buttons get red buttons.
//
// pHotbuttons = 0   small arrow buttons are hot
// pHotbuttons = 1   medium arrow buttons are hot
// pHotbuttons = 2   large arrow buttons are hot
//
// NOTE: This function checks to make sure that the hot button set has
// changed or else it exits.  This is so program function is not slowed by
// reloading the icons with each click.  To force a change, set the class
// variable currentHotButtons to -1 before calling.
//

public void setHotButtons(int pHotButtons)
{

//do nothing unless there is a change of the hot button set
if (pHotButtons == currentHotButtons) return;

//set all the large arrow buttons - if hot they get green arrows and are
//activated by alt-uparrow, etc.
if (pHotButtons == 2){
    uuu.setIcon(greenUpLargeArrow);
    lll.setIcon(greenLeftLargeArrow);
    rrr.setIcon(greenRightLargeArrow);
    ddd.setIcon(greenDownLargeArrow);
    uuu.setMnemonic(KeyEvent.VK_UP);
    lll.setMnemonic(KeyEvent.VK_LEFT);
    rrr.setMnemonic(KeyEvent.VK_RIGHT);
    ddd.setMnemonic(KeyEvent.VK_DOWN);
    uuu.setToolTipText(largeTip + " (alt-up arrow)");
    lll.setToolTipText(largeTip + " (alt-left arrow)");
    rrr.setToolTipText(largeTip + " (alt-right arrow)");
    ddd.setToolTipText(largeTip + " (alt-down arrow)");
    }
else {
    uuu.setIcon(redUpLargeArrow);
    lll.setIcon(redLeftLargeArrow);
    rrr.setIcon(redRightLargeArrow);
    ddd.setIcon(redDownLargeArrow);
    uuu.setMnemonic(KeyEvent.VK_UNDEFINED);
    lll.setMnemonic(KeyEvent.VK_UNDEFINED);
    rrr.setMnemonic(KeyEvent.VK_UNDEFINED);
    ddd.setMnemonic(KeyEvent.VK_UNDEFINED);
    uuu.setToolTipText(largeTip);
    lll.setToolTipText(largeTip);
    rrr.setToolTipText(largeTip);
    ddd.setToolTipText(largeTip);
    }

//set all the medium arrow buttons - if hot they get green arrows and are
//activated by alt-uparrow, etc.

if (pHotButtons == 1){
    uu.setIcon(greenUpMediumArrow);
    ll.setIcon(greenLeftMediumArrow);
    rr.setIcon(greenRightMediumArrow);
    dd.setIcon(greenDownMediumArrow);
    uu.setMnemonic(KeyEvent.VK_UP);
    ll.setMnemonic(KeyEvent.VK_LEFT);
    rr.setMnemonic(KeyEvent.VK_RIGHT);
    dd.setMnemonic(KeyEvent.VK_DOWN);
    uu.setToolTipText(mediumTip + " (alt-up arrow)");
    ll.setToolTipText(mediumTip + " (alt-left arrow)");
    rr.setToolTipText(mediumTip + " (alt-right arrow)");
    dd.setToolTipText(mediumTip + " (alt-down arrow)");
    }
else {
    uu.setIcon(redUpMediumArrow);
    ll.setIcon(redLeftMediumArrow);
    rr.setIcon(redRightMediumArrow);
    dd.setIcon(redDownMediumArrow);
    uu.setMnemonic(KeyEvent.VK_UNDEFINED);
    ll.setMnemonic(KeyEvent.VK_UNDEFINED);
    rr.setMnemonic(KeyEvent.VK_UNDEFINED);
    dd.setMnemonic(KeyEvent.VK_UNDEFINED);
    uu.setToolTipText(mediumTip);
    ll.setToolTipText(mediumTip);
    rr.setToolTipText(mediumTip);
    dd.setToolTipText(mediumTip);
    }

//set all the small arrow buttons - if hot they get green arrows and are
//activated by alt-uparrow, etc.

if (pHotButtons == 0){
    u.setIcon(greenUpSmallArrow);
    l.setIcon(greenLeftSmallArrow);
    r.setIcon(greenRightSmallArrow);
    d.setIcon(greenDownSmallArrow);
    u.setMnemonic(KeyEvent.VK_UP);
    l.setMnemonic(KeyEvent.VK_LEFT);
    r.setMnemonic(KeyEvent.VK_RIGHT);
    d.setMnemonic(KeyEvent.VK_DOWN);
    u.setToolTipText(smallTip + " (alt-up arrow)");
    l.setToolTipText(smallTip + " (alt-left arrow)");
    r.setToolTipText(smallTip + " (alt-right arrow)");
    d.setToolTipText(smallTip + " (alt-down arrow)");
    }
else {
    u.setIcon(redUpSmallArrow);
    l.setIcon(redLeftSmallArrow);
    r.setIcon(redRightSmallArrow);
    d.setIcon(redDownSmallArrow);
    u.setMnemonic(KeyEvent.VK_UNDEFINED);
    l.setMnemonic(KeyEvent.VK_UNDEFINED);
    r.setMnemonic(KeyEvent.VK_UNDEFINED);
    d.setMnemonic(KeyEvent.VK_UNDEFINED);
    u.setToolTipText(smallTip);
    l.setToolTipText(smallTip);
    r.setToolTipText(smallTip);
    d.setToolTipText(smallTip);
    }

currentHotButtons = pHotButtons;

}//end of ArrowPanel::setHotButtons
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ArrowPanel::rotateHotButtons
//
// Changes the hotButtons to the next set.
//
// If currentHotButtons is still -1, small buttons will be set to hot.
//

public void rotateHotButtons()
{

//move to the next set of buttons
int newHotButtons = currentHotButtons + 1;

//verify in range
if (newHotButtons < 0) newHotButtons = 0;
if (newHotButtons > 2) newHotButtons = 0;

setHotButtons(newHotButtons);

}//end of ArrowPanel::rotateHotButtons
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ArrowPanel::setAllEnable
//
// Enables or disables all controls on the panel.
//

public void setAllEnable(boolean pEnable)
{

homeButton.setEnabled(pEnable);
centerButton.setEnabled(pEnable);
rotateHotButtons.setEnabled(pEnable);
uuu.setEnabled(pEnable); uu.setEnabled(pEnable); u.setEnabled(pEnable);
ddd.setEnabled(pEnable); dd.setEnabled(pEnable); d.setEnabled(pEnable);
lll.setEnabled(pEnable); ll.setEnabled(pEnable); l.setEnabled(pEnable);
rrr.setEnabled(pEnable); rr.setEnabled(pEnable); r.setEnabled(pEnable);

}//
//end of ArrowPanel::setAllEnable
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ArrowPanel::createImageIcon
//
// Returns an ImageIcon, or null if the path was invalid.
//
// Note: The tutorial suggests the following be used to resolve the path:
//
// java.net.URL imgURL = XYPositioner.class.getResource("path");
//
// The above code required path to point to the gif file from where the
// class directory was located, i.e
// C:\Public\Java Projects\BeamProfiler\src\beamprofiler\mksystems\hardware
// This didn't make sense as the code might be distributed in a jar and
// this directory might not be created on the target computer.
//
// ***************************************************************************
// NOTE: You must use forward slashes in the path names for the resource
// loader to find the image files in the JAR package.
// ***************************************************************************
//

protected static ImageIcon createImageIcon(String path)
{

java.net.URL imgURL = ArrowPanel.class.getResource(path);

if (imgURL != null) {
    return new ImageIcon(imgURL);
    }
else {return null;}

}//end of ArrowPanel::createImageIcon
//-----------------------------------------------------------------------------

}//end of class ArrowPanel
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class MJButton
//
// Provides a JButton with oft used settings.

class MJButton extends JButton{

//-----------------------------------------------------------------------------
// MJButton::MJButton (constructor)
//

public MJButton(String pText, int pMnemonic, String pToolTipText )
{

//transfer parameters to object variables
setText(pText); setMnemonic(pMnemonic);
//apply common settings
setup(pToolTipText);

}//end of MJButton::MJButton (constructor)
//-----------------------------------------------------------------------------

public MJButton(String pText, Icon pIcon, int pMnemonic, String pToolTipText )
{

//transfer parameters to object variables
setText(pText); setMnemonic(pMnemonic); setIcon(pIcon);
//apply common settings
setup(pToolTipText);

}//end of MJButton::MJButton (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MJButton::setup
//
// Applies common settings.
//

public final void setup(String pToolTipText)
{

setToolTipText(pToolTipText);
setVerticalTextPosition(AbstractButton.CENTER);
setHorizontalTextPosition(AbstractButton.CENTER);
setMargin(new Insets(2,2,2,2));
setMaximumSize(new Dimension(25,25));

}//end of MJButton::setup
//-----------------------------------------------------------------------------

}//end of class MJButton
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
