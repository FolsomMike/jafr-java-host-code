/******************************************************************************
* Title: WorldView.java - Data for the world!
* Author: Mike Schoonover
* Date: 10/31/15
*
* Purpose:
*
* This class handles data related to the world: loads, saves, etc.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package view;

//-----------------------------------------------------------------------------

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

//-----------------------------------------------------------------------------
// class WorldView

public class WorldView extends Object{

    private String dataVersion = "1.0";

    int maxX, maxY;

    private final ArrayList <SpriteView> spriteViews = new ArrayList<>(5);

    static final int DATA_SIZE = 1000;

//-----------------------------------------------------------------------------
// WorldView::WorldView (constructor)
//

public WorldView(int pMaxX, int pMaxY)
{

    maxX = pMaxX; maxY = pMaxY;

}//end of WorldView::WorldView (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// WorldView::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{

    setUpUniverse();

}//end of WorldView::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// WorldView::setUpUniverse
//
// Sets up the universe and all its inhabitants.
//

public void setUpUniverse()
{

    TaterBotView taterBot = new TaterBotView(0);
    taterBot.init();
    taterBot.setXY(maxX/2, maxY/2);
    taterBot.setUTSweepLineAngle(90);
    taterBot.setColor(Color.BLACK);
    spriteViews.add(taterBot);

    spriteViews.add(taterBot);

/*

    for (int i=0; i<DATA_SIZE; i++){
        TaterBotView taterBot = new TaterBotView(i);
        taterBot.init();
        taterBot.setXY(
                    (int)(Math.random() * maxX), (int)(Math.random() * maxY));

        taterBot.setColor(new Color((int)(Math.random() * 255),
                      (int)(Math.random() * 255),
                      (int)(Math.random() * 255),
                      255));
        spriteViews.add(taterBot);
    }
*/


}//end of WorldView::setUpUniverse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// WorldView::setBotUTSweepLineAngle
//
// Sets the UT Sweep Line Angle for the sprite specified by pIndex to pAngle.
//

public void setBotUTSweepLineAngle(int pIndex, int pAngle)
{

    spriteViews.get(pIndex).setUTSweepLineAngle(pAngle);

}//end of WorldView::setBotUTSweepLineAngle
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// WorldView::draw
//
// Draws all the objects in the universe.
//

public void draw(Graphics2D pG2)
{

    for (SpriteView spriteView : spriteViews) { spriteView.draw(pG2); }

}//end of WorldView::draw
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// WorldView::getDataVersion
//
// Returns dataVersion.
//

public String getDataVersion()
{

    return dataVersion;

}//end of WorldView::getDataVersion
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// WorldView::setDataVersion
//
// Sets dataVersion to pValue.
//

public void setDataVersion(String pValue)
{

    dataVersion = pValue;

}//end of WorldView::setDataVersion
//-----------------------------------------------------------------------------

}//end of ViewWorld
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
