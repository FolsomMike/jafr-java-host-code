/******************************************************************************
* Title: SpriteView.java - the parent class for all screen objects
* Author: Mike Schoonover
* Date: 1/21/17
*
* Purpose:
*
* This class is the parent class for all things which exist on the display.
* There is a mirror class in the model package to handle related data.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

package view;

//-----------------------------------------------------------------------------

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

//-----------------------------------------------------------------------------
// class SpriteView
//

public class SpriteView extends Object{

    int idNum;
    boolean alive;
    int x, y, centerX, centerY;
    int size;
    int width, height;
    Color color;

    Ellipse2D ellipse;
    Rectangle2D rectangle;

    Line2D.Double utSweepLine;

    int utSweepLineAngle = 90;
    int utSweepLineLength = 20;
    int utSweepLineBaseX = 0, utSweepLineBaseY = 0;
    int utSweepLineEndX = 0, utSweepLineEndY = 0;

    public int getIDNum(){ return(idNum); }
    public boolean isAlive(){ return(alive); }
    public int getX(){ return(x); }
    public int getY(){ return(y); }
    public int getCenterX(){ return(centerX); }
    public int getCenterY(){ return(centerY); }
    public int getWidth(){ return(width); }
    public int getHeight(){ return(height); }
    public Rectangle2D getRectangle(){ return(rectangle); }

//-----------------------------------------------------------------------------
// SpriteView::SpriteView (constructor)
//

public SpriteView(int pIDNum)
{

    idNum = pIDNum;

}//end of SpriteView::SpriteView (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{

    alive = true;

    size = 5; width = size; height = size;

    updateShape();

}//end of SpriteView::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::updateShape
//
// Updates the shape and the bounding rectangle to reflect new x,y,width,height.
//

void updateShape()
{

    ellipse = new Ellipse2D.Double(x, y, width, height);
    rectangle = new Rectangle2D.Double(x, y, width, height);

}//end of SpriteView::updateShape
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::draw
//
// Draws the sprite.
//

public void draw(Graphics2D pG2)
{

    if (!alive){ return; }

    pG2.setColor(color);
    pG2.draw(ellipse);

}//end of SpriteView::draw
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::setColor
//
// Sets the color.
//

public void setColor(Color pColor)
{

    color = pColor;

}//end of SpriteView::setColor
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::setXY
//
// Sets the x,y location of the SpriteView to pX,pY and updates related values.
//

public void setXY(int pX, int pY)
{

    x = pX; y = pY;

    calculateCenter();

    utSweepLineBaseX = centerX; utSweepLineBaseY = centerY;

    updateShape();

}//end of SpriteView::setXY
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::setCenter
//
// Sets the center location of the SpriteView to pX,pY and updates related
// values.
//

private void setCenter(int pX, int pY)
{

    centerX = pX; centerY = pY;

    calculateXYFromCenter();

}//end of SpriteView::setXY
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::setUTSweepLineAngle
//
// Sets the angle of the UT sweep line.
//

public void setUTSweepLineAngle(int pAngle)
{

    utSweepLineAngle = pAngle;

    calculateUTSweepLineCoords();

    updateShape();

}//end of SpriteView::setUTSweepLineAngle
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::calculateUTSweepLineCoords
//
// Calculates the coordiantes of the UT sweep line based on the basepoint and
// the angle.
//

public void calculateUTSweepLineCoords()
{


}//end of SpriteView::calculateUTSweepLineCoords
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::calculateCenter
//
// Calculates the center point of the SpriteView from the x,y position.
//

void calculateCenter()
{

    centerX = x + size/2; centerY = y + size/2;

}//end of SpriteView::calculateCenter
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::calculateXYFromCenter
//
// Calculates the x,y position of the SpriteView from its center point.
//

private void calculateXYFromCenter()
{

    x = centerX - size/2; y = centerY - size/2;

}//end of SpriteView::calculateXYFromCenter
//-----------------------------------------------------------------------------

}//end of class SpriteView
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
