/******************************************************************************
* Title: RobotView.java - the parent class for all robots
* Author: Mike Schoonover
* Date: 1/21/17
*
* Purpose:
*
* This class is the parent class for all robots.
* There is a mirror class in the model package to handle related data.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

package view;

//-----------------------------------------------------------------------------

import java.awt.Graphics2D;

//-----------------------------------------------------------------------------
// class RobotView
//

public class RobotView extends SpriteView{
    
//-----------------------------------------------------------------------------
// RobotView::RobotView (constructor)
//

public RobotView(int pIDNum)
{

    super(pIDNum);
        
}//end of RobotView::RobotView (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotView::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

@Override
public void init()
{

    super.init();

}//end of RobotView::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotView::draw
//
// Draws the robot.
//

@Override
public void draw(Graphics2D pG2)
{

    super.draw(pG2);

}//end of RobotView::draw
//-----------------------------------------------------------------------------


}//end of class RobotView
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
