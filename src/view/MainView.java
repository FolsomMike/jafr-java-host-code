/******************************************************************************
* Title: MainView.java
* Author: Mike Schoonover
* Date: 3/12/13
*
* Purpose:
*
* This class is the Main View in a Model-View-Controller architecture.
* It creates and handles all GUI components.
* It knows about the Controller.
*
* There may be many classes in the view package which handle different aspects
* of the GUI.
*
* All GUI control events, including Timer events are caught by this object
* and passed on to the "Controller" object pointed by the class member
* "eventHandler" for final handling.
*
*/

//-----------------------------------------------------------------------------

package view;

//-----------------------------------------------------------------------------

import controller.EventHandler;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import javax.swing.AbstractAction;
import static javax.swing.Action.MNEMONIC_KEY;
import static javax.swing.Action.SHORT_DESCRIPTION;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import mksystems.mswing.MFloatSpinner;
import toolkit.Tools;

//-----------------------------------------------------------------------------
// class MainView
//

public class MainView implements
        ActionListener, WindowListener, ChangeListener, MouseWheelListener {

    private final static int UNIVERSE_WIDTH = 735;
    private final static int UNIVERSE_HEIGHT = -1;

    private final static int STATUS_X_POS = 852;
    private final static int STATUS_Y_POS = 395;

    private final static int LOG_X_POS = 1000;
    private final static int LOG_Y_POS = 395;

    private JFrame mainFrame;
    private JPanel mainPanel;

    UniversePanel universePanel;

    WorldView worldView;

    private MainMenu mainMenu;

    private JSlider headAngleSlider;

    private JTextField dataVersionTField;
    private JTextField dataTArea1;
    private JTextField dataTArea2;

    private Status status;

    private GuiUpdater guiUpdater;
    private Log log;
    private ThreadSafeLogger tsLog;
    private Help help;
    private About about;

    private javax.swing.Timer mainTimer;

    private final EventHandler eventHandler;

    private Font blackSmallFont, redSmallFont;
    private Font redLargeFont, greenLargeFont, yellowLargeFont, blackLargeFont;

    private JLabel statusLabel, spriteCountLabel;
    private JLabel largestSpriteSizeLabel;
    private JLabel infoLabel;
    private JLabel progressLabel;

    JCheckBox autoRestartChkBox;

    Dimension totalScreenSize, usableScreenSize;
    int universeMaxX, universeMaxY;

    public static final int EXAMPLE_CONSTANT = 0;

    public int getUniverseMaxX(){ return(universeMaxX); }
    public int getUniverseMaxY(){ return(universeMaxY); }


class UpAction extends AbstractAction {

    public UpAction(String pName, Integer pMnemonic) {
        super(pName);
        putValue(SHORT_DESCRIPTION, "Move Forward");
        putValue(MNEMONIC_KEY, pMnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        eventHandler.actionPerformed(
                new ActionEvent(e.getSource(), e.getID(), "Move Up Large"));
    }

}//end of class UpAction

class DownAction extends AbstractAction {

    public DownAction(String pName, Integer pMnemonic) {
        super(pName);
        putValue(SHORT_DESCRIPTION, "Move Reverse");
        putValue(MNEMONIC_KEY, pMnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        eventHandler.actionPerformed(
                new ActionEvent(e.getSource(), e.getID(), "Move Down Large"));
    }

}//end of class DownAction

class LeftAction extends AbstractAction {

    public LeftAction(String pName, Integer pMnemonic) {
        super(pName);
        putValue(SHORT_DESCRIPTION, "Move Left");
        putValue(MNEMONIC_KEY, pMnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        eventHandler.actionPerformed(
                new ActionEvent(e.getSource(), e.getID(), "Move Left Large"));
    }

}//end of class LeftAction

class RightAction extends AbstractAction {

    public RightAction(String pName, Integer pMnemonic) {
        super(pName);
        putValue(SHORT_DESCRIPTION, "Move Right");
        putValue(MNEMONIC_KEY, pMnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        eventHandler.actionPerformed(
                new ActionEvent(e.getSource(), e.getID(), "Move Right Large"));
    }

}//end of class RightAction


//-----------------------------------------------------------------------------
// MainView::MainView (constructor)
//

public MainView(EventHandler pEventHandler)
{

    eventHandler = pEventHandler;

}//end of MainView::MainView (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{

    setupMainFrame();

    setupHotKeys();

    //create an object to handle thread safe updates of GUI components
    guiUpdater = new GuiUpdater(mainFrame);
    guiUpdater.init();

    //add a menu to the main form, passing this as the action listener
    mainFrame.setJMenuBar(mainMenu = new MainMenu(this));

    //create various fonts for use by the program
    createFonts();

    //create user interface: buttons, displays, etc.
    setupGui();

    worldView = new WorldView(getUniverseMaxX(), getUniverseMaxY());
    worldView.init();

    //arrange all the GUI items
    mainFrame.pack();

    //display the main frame
    mainFrame.setVisible(true);

}// end of MainView::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createLogger
//
// Creates the log window and a theadsafe logger object.
//
// Returns the threadsafe logger.
//

public ThreadSafeLogger createLogger()
{

    log = new Log(mainFrame); log.setLocation(LOG_X_POS, LOG_Y_POS);

    log.setVisible(true);

    tsLog = new ThreadSafeLogger(log.textArea);

    return(tsLog);

}// end of MainView::createLogger
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::getScreenSize
//
// Retrieves the current screen size along with the actual usable vertical
// size after subtracting the size of the taskbar.
//
// Returns the usable size as a Dimension.
//

private Dimension getScreenSize()
{

    totalScreenSize = Toolkit.getDefaultToolkit().getScreenSize();

    //height of the task bar
    Insets scnMax = Toolkit.getDefaultToolkit().getScreenInsets(
                                        mainFrame.getGraphicsConfiguration());
    int taskBarHeight = scnMax.bottom;

    usableScreenSize = new Dimension(
                  totalScreenSize.width, totalScreenSize.height-taskBarHeight);

    return(usableScreenSize);

}// end of MainView::getScreenSize
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setBotUTSweepLineAngle
//
// Sets the UT Sweep Line Angle for the sprite specified by pIndex to pAngle.
//

public void setBotUTSweepLineAngle(int pIndex, int pAngle)
{

    worldView.setBotUTSweepLineAngle(pIndex, pAngle);

}//end of MainView::setBotUTSweepLineAngle
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupMainFrame
//
// Sets various options and styles for the main frame.
//

public void setupMainFrame()
{

    mainFrame = new JFrame("JaFR Central Command");

    getScreenSize();

    //add a JPanel to the frame to provide a familiar container
    mainPanel = new JPanel();
    mainFrame.setContentPane(mainPanel);

    mainFrame.addWindowListener(this);

    //turn off default bold for Metal look and feel
    UIManager.put("swing.boldMetal", Boolean.FALSE);

    //force "look and feel" to Java style
    try {
        UIManager.setLookAndFeel(
            UIManager.getCrossPlatformLookAndFeelClassName());
        }
    catch (ClassNotFoundException | InstantiationException |
            IllegalAccessException | UnsupportedLookAndFeelException e) {
        System.out.println("Could not set Look and Feel");
        }

    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    //    setLocation((int)screenSize.getWidth() - getWidth(), 0);

    status = new Status(mainFrame, STATUS_X_POS, STATUS_Y_POS);

}// end of MainView::setupMainFrame
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupHotKeys
//
// Sets up hot keys to control various functions.
//

private void setupHotKeys()
{

    JRootPane rootPane = mainFrame.getRootPane();

    UpAction upAction = new UpAction("Move Forward", KeyEvent.VK_UP);

    rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
       put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.SHIFT_MASK),
       "Move Forward");
    rootPane.getActionMap().put("Move Forward", upAction);

    DownAction downAction = new DownAction("Move Reverse", KeyEvent.VK_DOWN);

    rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
       put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.SHIFT_MASK),
       "Move Reverse");
    rootPane.getActionMap().put("Move Reverse", downAction);

    LeftAction leftAction = new LeftAction("Move Left", KeyEvent.VK_LEFT);

    rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
       put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.SHIFT_MASK),
       "Move Left");
    rootPane.getActionMap().put("Move Left", leftAction);

    RightAction rightAction = new RightAction("Move Right", KeyEvent.VK_RIGHT);

    rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
       put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.SHIFT_MASK),
       "Move Right");
    rootPane.getActionMap().put("Move Right", rightAction);

}// end of MainView::setupHotKeys
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupGUI
//
// Sets up the user interface on the mainPanel: buttons, displays, etc.
//

private void setupGui()
{

    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));

    mainPanel.add(createLeftPanel());

    mainPanel.add(createRightPanel());

}// end of MainView::setupGui
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createLeftPanel
//
// Returns a JPanel for use on the left of the screen for holding controls,
// status messages, etc.
//

private JPanel createLeftPanel()
{

    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));


    panel.add(createControlsPanel());

    ArrowPanel arrowPanel;

    panel.add(arrowPanel = new ArrowPanel(this, 1, 2, 3));
    arrowPanel.init();

    panel.add(Box.createVerticalGlue());

    return(panel);

}// end of MainView::createLeftPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createControlsPanel
//
// Returns a JPanel containing the control GUI items.
//

private JPanel createControlsPanel()
{

    JPanel panel = new JPanel();
    panel.setBorder(BorderFactory.createTitledBorder("Controls"));
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);

    addVerticalSpacer(panel, 20);

    panel.add(createAliveSpriteCountPanel());
    panel.add(createLargestSpriteSizePanel());

    addVerticalSpacer(panel, 5);

    panel.add(createAutoRestartPanel());

    addVerticalSpacer(panel, 20);

    //create a label to display miscellaneous info
    infoLabel = new JLabel("Info");
    infoLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
    panel.add(infoLabel);

    addVerticalSpacer(panel, 20);

    //add text field
    dataVersionTField = new JTextField("unknown");
    dataVersionTField.setAlignmentX(Component.LEFT_ALIGNMENT);
    Tools.setSizes(dataVersionTField, 100, 24);
    //text fields don't have action commands or action listeners
    dataVersionTField.setToolTipText("The data format version.");
    panel.add(dataVersionTField);

    addVerticalSpacer(panel, 3);

    //add text field
    dataTArea1 = new JTextField("");
    dataTArea1.setAlignmentX(Component.LEFT_ALIGNMENT);
    Tools.setSizes(dataTArea1, 100, 24);
    //text fields don't have action commands or action listeners
    dataTArea1.setToolTipText("A data entry.");
    panel.add(dataTArea1);

    addVerticalSpacer(panel, 3);

    //add text field
    dataTArea2 = new JTextField("");
    dataTArea2.setAlignmentX(Component.LEFT_ALIGNMENT);
    Tools.setSizes(dataTArea2, 100, 24);
    //text fields don't have action commands or action listeners
    dataTArea2.setToolTipText("A data entry.");
    panel.add(dataTArea2);

    addVerticalSpacer(panel, 20);

    //add button
    JButton loadBtn = new JButton("Load");
    loadBtn.setAlignmentX(Component.LEFT_ALIGNMENT);
    loadBtn.setActionCommand("Load Data From File");
    loadBtn.addActionListener(this);
    loadBtn.setToolTipText("Load data from file.");
    panel.add(loadBtn);

    addVerticalSpacer(panel, 10);

    //add a button
    JButton saveBtn = new JButton("Save");
    saveBtn.setAlignmentX(Component.LEFT_ALIGNMENT);
    saveBtn.setActionCommand("Save Data To File");
    saveBtn.addActionListener(this);
    saveBtn.setToolTipText("Save data to file.");
    panel.add(saveBtn);

    addVerticalSpacer(panel, 10);

    progressLabel = new JLabel("Progress");
    progressLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
    panel.add(progressLabel);

    addVerticalSpacer(panel, 10);

    //set this spinner up for use with doubles
    //the format string "##0.0" has decimal places
    //use intSpinner1.getDoubleValue() to retrieve the value as an integer

    MFloatSpinner doubleSpinner1 =
            new MFloatSpinner(5.5, 1.1, 9.9, 0.1, "##0.0", 60, 20);
    doubleSpinner1.setName("Double Spinner 1 -- used for doubles");
    doubleSpinner1.addChangeListener(this);
    doubleSpinner1.setToolTipText("This is float spinner #1!");
    doubleSpinner1.setAlignmentX(Component.LEFT_ALIGNMENT);
    panel.add(doubleSpinner1);

    addVerticalSpacer(panel, 10);

    //set this spinner up for use with integers
    //the format string "##0" has no decimal places
    //use intSpinner1.getIntValue() to retrieve the value as an integer

    MFloatSpinner intSpinner1 =
            new MFloatSpinner(1, 1, 100000, 1, "##0", 60, 20);
    intSpinner1.setName("Integer Spinner 1 -- used for integers");
    intSpinner1.addChangeListener(this);
    intSpinner1.setToolTipText("This is float spinner #1!");
    intSpinner1.setAlignmentX(Component.LEFT_ALIGNMENT);
    panel.add(intSpinner1);


    addVerticalSpacer(panel, 10);

    JPanel sweepPanel = new JPanel();
    sweepPanel.setLayout(new BoxLayout(sweepPanel, BoxLayout.Y_AXIS));
    sweepPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

    //add a button
    JButton oneFullSweepBtn = new JButton("One Sweep");
    oneFullSweepBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
    oneFullSweepBtn.setMargin(new Insets(0, 0, 0, 0));
    oneFullSweepBtn.setActionCommand("One Sweep");
    oneFullSweepBtn.addActionListener(this);
    oneFullSweepBtn.setToolTipText("Performs one full sweep of the head.");
    sweepPanel.add(oneFullSweepBtn);

    addVerticalSpacer(sweepPanel, 3);

    //add a button
    JButton repeatFullSweepBtn = new JButton("Repeat Sweep");
    repeatFullSweepBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
    repeatFullSweepBtn.setMargin(new Insets(0, 0, 0, 0));
    repeatFullSweepBtn.setActionCommand("Repeat Sweep");
    repeatFullSweepBtn.addActionListener(this);
    repeatFullSweepBtn.setToolTipText("Repeats full sweep of the head.");
    sweepPanel.add(repeatFullSweepBtn);

    addVerticalSpacer(sweepPanel, 3);

    //add a button
    JButton stopSweepBtn = new JButton("Stop Sweep");
    stopSweepBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
    stopSweepBtn.setMargin(new Insets(0, 0, 0, 0));
    stopSweepBtn.setActionCommand("Stop Sweep");
    stopSweepBtn.addActionListener(this);
    stopSweepBtn.setToolTipText("Stops sweep of the head.");
    sweepPanel.add(stopSweepBtn);

    panel.add(sweepPanel);

    return(panel);

}// end of MainView::createControlsPanel
//-----------------------------------------------------------------------------

static final int HEAD_ANGLE_MIN = 0;
static final int HEAD_ANGLE_MAX = 180;
static final int HEAD_ANGLE_INIT = 90;

//-----------------------------------------------------------------------------
// MainView::createHeadAngleSlider
//
// Returns a Slider to control the angular position of the robot's head.
//

private JSlider createHeadAngleSlider(){

headAngleSlider = new JSlider(JSlider.HORIZONTAL,
                              HEAD_ANGLE_MIN, HEAD_ANGLE_MAX, HEAD_ANGLE_INIT);

headAngleSlider.addChangeListener(this);
headAngleSlider.addMouseWheelListener(this);

headAngleSlider.setName("Head Angle Slider");

headAngleSlider.setInverted(true); //minimum is on the left

//turn on labels at major tick marks.
headAngleSlider.setMajorTickSpacing(10);
headAngleSlider.setMinorTickSpacing(1);
headAngleSlider.setPaintTicks(true);
headAngleSlider.setPaintLabels(true);

return(headAngleSlider);

}// end of MainView::createHeadAngleSlider
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createAliveSpriteCountPanel
//
// Returns a JPanel displaying the alive sprite count status.
//

private JPanel createAliveSpriteCountPanel()
{

    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);
    JLabel label = new JLabel("Sprite Count: ");
    panel.add(label);
    spriteCountLabel = new JLabel("1000");
    panel.add(spriteCountLabel);

    return(panel);

}// end of MainView::createAliveSpriteCountPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createLargestSpriteSizePanel
//
// Returns a JPanel displaying the size of the largest living Sprite.
//

private JPanel createLargestSpriteSizePanel()
{

    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);
    JLabel label = new JLabel("Largest Size: ");
    panel.add(label);
    largestSpriteSizeLabel = new JLabel("1");
    panel.add(largestSpriteSizeLabel);

    return(panel);

}// end of MainView::createLargestSpriteSizePanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createAutoRestartPanel
//
// Returns a JPanel displaying a control to allow selection of Auto Restart
// mode.
//

private JPanel createAutoRestartPanel()
{

    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);

    autoRestartChkBox = new JCheckBox("Auto Restart");
    autoRestartChkBox.setSelected(true);
    panel.add(autoRestartChkBox);

    return(panel);

}// end of MainView::createAutoRestartPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createControlsPanel
//
// Returns a JPanel containing the control GUI items.
//

private JPanel createUniversePanel()
{

    int universeWidth, universeHeight;

    if (UNIVERSE_WIDTH != -1){
        universeWidth = UNIVERSE_WIDTH;
    }else{
        universeWidth = usableScreenSize.width - 130;
    }

    if (UNIVERSE_HEIGHT != -1){
        universeHeight = UNIVERSE_HEIGHT;
    }else{
        universeHeight = usableScreenSize.height - 118;
    }

    universeMaxX = universeWidth - 1;
    universeMaxY = universeHeight - 1;

    universePanel = new UniversePanel(1, this, this,
                                                universeWidth, universeHeight);

    universePanel.init();

    return(universePanel);

}// end of MainView::createUniversePanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createRightPanel
//
// Returns a JPanel for use on the right of the screen for holding controls
// and the visual display.
//

private JPanel createRightPanel()
{

    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    panel.add(createUniversePanel());

    panel.add(createBottomControlsPanel());

    return(panel);

}// end of MainView::createLeftPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createBottomControlsPanel
//
// Returns a JPanel for use at the bottom of the right panel.
//

private JPanel createBottomControlsPanel()
{

    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

    panel.add(createHeadAngleSlider());

    return(panel);

}// end of MainView::createBottomControlsPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::getValueOfHeadAngleSlider
//
// Returns the current value of the component.
//

public int getValueOfHeadAngleSlider()
{

    return(headAngleSlider.getValue());

}// end of MainView::getValueOfHeadAngleSlider
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setValueOfHeadAngleSlider
//
// Sets the current value of the component.
//

public void setValueOfHeadAngleSlider(int pValue)
{

    headAngleSlider.setValue(pValue);

}// end of MainView::setValueOfHeadAngleSlider
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::addVerticalSpacer
//
// Adds a vertical spacer of pNumPixels height to JPanel pTarget.
//

private void addVerticalSpacer(JPanel pTarget, int pNumPixels)
{

    pTarget.add(Box.createRigidArea(new Dimension(0,pNumPixels)));

}// end of MainView::addVerticalSpacer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::updateUniverseDisplay
//
// Updates the Universe display panel.
//

public void updateUniverseDisplay()
{

    mainPanel.repaint();

}//end of MainView::updateUniverseDisplay
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::draw
//
// Draws the universe.
//

public void draw(Graphics2D pG2)
{

    worldView.draw(pG2);

}//end of MainView::draw
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createFonts
//
// Creates fonts for use by the program.
//

public void createFonts()
{

    //create small and large red and green fonts for use with display objects
    HashMap<TextAttribute, Object> map = new HashMap<>();

    blackSmallFont = new Font("Dialog", Font.PLAIN, 12);

    map.put(TextAttribute.FOREGROUND, Color.RED);
    redSmallFont = blackSmallFont.deriveFont(map);

    //empty the map to use for creating the large fonts
    map.clear();

    blackLargeFont = new Font("Dialog", Font.PLAIN, 20);

    map.put(TextAttribute.FOREGROUND, Color.GREEN);
    greenLargeFont = blackLargeFont.deriveFont(map);

    map.put(TextAttribute.FOREGROUND, Color.RED);
    redLargeFont = blackLargeFont.deriveFont(map);

    map.put(TextAttribute.FOREGROUND, Color.YELLOW);
    yellowLargeFont = blackLargeFont.deriveFont(map);

}// end of MainView::createFonts
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayLog
//
// Displays the log window. It is not released after closing as the information
// is retained so it can be viewed the next time the window is opened.
//

public void displayLog()
{

    log.setVisible(true);

}//end of MainView::displayLog
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayHelp
//
// Displays help information.
//

public void displayHelp()
{

    help = new Help(mainFrame);
    help = null;  //window will be released on close, so point should be null

}//end of MainView::displayHelp
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayAbout
//
// Displays about information.
//

public void displayAbout()
{

    about = new About(mainFrame);
    about = null;  //window will be released on close, so point should be null

}//end of MainView::displayAbout
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayErrorMessage
//
// Displays an error dialog with message pMessage.
//

public void displayErrorMessage(String pMessage)
{

    Tools.displayErrorMessage(pMessage, mainFrame);

}//end of MainView::displayErrorMessage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::updateGUIDataSet1
//
// Updates some of the GUI with data.
//

public void updateGUIDataSet1()
{


}//end of MainView::updateGUIDataSet1
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::drawRectangle
//
// Draws a rectangle on mainPanel
//

public void drawRectangle()
{


    Graphics2D g2 = (Graphics2D)mainPanel.getGraphics();

     // draw Rectangle2D.Double
    g2.draw(new Rectangle2D.Double(20, 10,10, 10));

}//end of MainView::drawRectangle
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::updateModelDataSet1
//
// Updates some of the model data with values in the GUI.
//

public void updateModelDataSet1()
{

}//end of MainView::updateModelDataSet1
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupAndStartMainTimer
//
// Prepares and starts a Java Swing timer.
//

public void setupAndStartMainTimer()
{

    //main timer has 2 second period
    mainTimer = new javax.swing.Timer (10, this);
    mainTimer.setActionCommand ("Timer");
    mainTimer.start();

}// end of MainView::setupAndStartMainTimer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setTextForDataTArea1
//
// Sets the text value for text box.
//

public void setTextForDataTArea1(String pText)
{

    dataTArea1.setText(pText);

}// end of MainView::setTextForDataTArea1
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setTextForDataTArea2
//
// Sets the text value for text box.
//

public void setTextForDataTArea2(String pText)
{

    dataTArea2.setText(pText);

}// end of MainView::setTextForDataTArea2
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::actionPerformed
//
// Responds to events and passes them on to the "Controller" (MVC Concept)
// objects.
//

@Override
public void actionPerformed(ActionEvent e)
{

    eventHandler.actionPerformed(e);

}//end of MainView::actionPerformed
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::mouseWheelMoved
//
// Handles movement of the mouse wheel.
//

@Override
public void mouseWheelMoved(MouseWheelEvent evt){

    if (evt.getWheelRotation() > 0 ){

        int iNewValue =
            headAngleSlider.getValue() - headAngleSlider.getMinorTickSpacing();

        if (iNewValue >= headAngleSlider.getMinimum()){
            headAngleSlider.setValue(iNewValue);
        }else{
            headAngleSlider.setValue(0);
        }
    }
    else{
        int iNewValue =
             headAngleSlider.getValue() + headAngleSlider.getMinorTickSpacing();

        if (iNewValue <= headAngleSlider.getMaximum()){
            headAngleSlider.setValue(iNewValue);
        }else{
            headAngleSlider.setValue(headAngleSlider.getMaximum());
        }
    }

}// end of MainView::mouseWheelMoved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::updateGUI
//
// Updates various GUI items such as status messages and labels.
//

public void updateGUI()
{


}//end of MainView::updateGUI
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::stateChanged
//

@Override
public void stateChanged(ChangeEvent ce) {

    eventHandler.stateChanged(ce);

}//end of MainView::stateChanged
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::windowClosing
//
// Handles actions necessary when the window is closing
//

@Override
public void windowClosing(WindowEvent e)
{

    eventHandler.windowClosing(e);

}//end of Controller::windowClosing
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::(various window listener functions)
//
// These functions are implemented per requirements of interface WindowListener
// but do nothing at the present time.  As code is added to each function, it
// should be moved from this section and formatted properly.
//

@Override
public void windowActivated(WindowEvent e){}
@Override
public void windowDeactivated(WindowEvent e){}
@Override
public void windowOpened(WindowEvent e){}
//@Override
//public void windowClosing(WindowEvent e){}
@Override
public void windowClosed(WindowEvent e){}
@Override
public void windowIconified(WindowEvent e){}
@Override
public void windowDeiconified(WindowEvent e){}

//end of MainView::(various window listener functions)
//-----------------------------------------------------------------------------

}//end of class MainView
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
