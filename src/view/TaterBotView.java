/******************************************************************************
* Title: TaterBotView.java - the parent class for all TaterBots
* Author: Mike Schoonover
* Date: 1/21/17
*
* Purpose:
*
* This class is the parent class for all TaterBots.
* There is a mirror class in the model package to handle related data.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

package view;

//-----------------------------------------------------------------------------

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

//-----------------------------------------------------------------------------
// class TaterBot
//

public class TaterBotView extends RobotView{

    Rectangle2D body;

//-----------------------------------------------------------------------------
// TaterBotView::TaterBotView (constructor)
//

public TaterBotView(int pIDNum)
{

    super(pIDNum);

}//end of TaterBotView::TaterBotView (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TaterBotView::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

@Override
public void init()
{

    super.init();

}//end of TaterBotView::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TaterBotView::updateShape
//
// Updates the shape and the bounding rectangle to reflect new x,y,width,height.
//

@Override
void updateShape()
{

    utSweepLine = new Line2D.Double(utSweepLineBaseX, utSweepLineBaseY,
                                            utSweepLineEndX, utSweepLineEndY);

    body = new Rectangle2D.Double(x, y, width, height);
    rectangle = new Rectangle2D.Double(x, y, width, height);

}//end of TaterBotView::updateShape
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TaterBotView::draw
//
// Draws the sprite.
//

@Override
public void draw(Graphics2D pG2)
{

    if (!alive){ return; }

    pG2.setColor(color);
    pG2.draw(body);
    pG2.draw(utSweepLine);

}//end of TaterBotView::draw
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpriteView::calculateUTSweepLineCoords
//
// Calculates the coordinates of the UT sweep line based on the basepoint and
// the angle.
//

@Override
public void calculateUTSweepLineCoords()
{

    int xOffset =
         (int)(Math.cos(Math.toRadians(utSweepLineAngle)) * utSweepLineLength);
    int yOffset =
         (int)(Math.sin(Math.toRadians(utSweepLineAngle)) * utSweepLineLength);

    utSweepLineEndX = utSweepLineBaseX + xOffset;
    utSweepLineEndY = utSweepLineBaseY - yOffset;

}//end of SpriteView::calculateUTSweepLineCoords
//-----------------------------------------------------------------------------


}//end of class TaterBotView
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
