/******************************************************************************
* Title: UniversePanel.java
* Author: Mike Schoonover
* Date: 4/23/16
*
* Purpose:
*
* This class handles the panel in which the Universe is drawn.
*
* 
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

package view;

//-----------------------------------------------------------------------------

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import toolkit.Tools;

//-----------------------------------------------------------------------------
// class UniversePanel
//

public class UniversePanel extends JPanel{
        
    int idNum;
    int maxX, maxY;
    
    MainView mainView;
    
    ActionListener actionListener;

//-----------------------------------------------------------------------------
// UniversePanel::UniversePanel (constructor)
//

public UniversePanel(int pIDNum, 
                     MainView pMainView,
                     ActionListener pActionListener,
                     int pMaxX, int pMaxY)
{

    idNum = pIDNum; 
    mainView = pMainView;
    actionListener = pActionListener;
    maxX = pMaxX; maxY = pMaxY;
        
}//end of UniversePanel::UniversePanel (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// UniversePanel::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{
    
    setBorder(BorderFactory.createTitledBorder("The Universe")); 
    Tools.setSizes(this, maxX + 2, maxY + 2);

}//end of UniversePanel::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// UniversePanel::paint
//
// Paints the Universe.
//

@Override
public void paint(Graphics g)
{

    Graphics2D g2 =(Graphics2D)g;
    
    draw(g2);    
    
    mainView.draw(g2);
    
}//end of UniversePanel::paint
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// UniversePanel::draw
//
// Draws the basic elements of the Universe.
//

public void draw(Graphics2D pG2)
{
    
}//end of UniversePanel::draw
//-----------------------------------------------------------------------------

}//end of class UniversePanel
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
    