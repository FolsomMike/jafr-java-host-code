/******************************************************************************
* Title: Status.java
* Author: Mike Schoonover
* Date: 11/04/17
*
* Purpose:
*
* This class displays a window for displaying status information.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package view;

import java.awt.*;
import javax.swing.*;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class Status
//
// This class displays a text area in a window.
//

class Status extends JDialog{

    JPanel mainPanel;

//-----------------------------------------------------------------------------
// Status::Status (constructor)
//

public Status(JFrame frame, int pXPos, int pYPos)
{

    super(frame, "Status");

    int panelWidth = 145;
    int panelHeight = 283;

    mainPanel = new JPanel();

    mainPanel.setMinimumSize(new Dimension(panelWidth, panelHeight));
    mainPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
    mainPanel.setMaximumSize(new Dimension(panelWidth, panelHeight));

    setContentPane(mainPanel);

    setLocation(pXPos, pYPos);

    setVisible(true);

    pack();

}//end of Status::Status (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Status::toHex4String
//
// Converts an integer to a 4 character hex string.
//

static String toHex4String(int pValue)
{

    String s = Integer.toString(pValue, 16);

    //force length to be four characters

    if (s.length() == 0) {return "0000" + s;}
    else
    if (s.length() == 1) {return "000" + s;}
    else
    if (s.length() == 2) {return "00" + s;}
    else
    if (s.length() == 3) {return "0" + s;}
    else{
        return s;
    }

}//end of Status::toHex4String
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Status::toHex8String
//
// Converts an integer to an 8 character hex string.
//

static String toHex8String(int pValue)
{

    String s = Integer.toString(pValue, 16);

    //force length to be eight characters

    if (s.length() == 0) {return "00000000" + s;}
    else
    if (s.length() == 1) {return "0000000" + s;}
    else
    if (s.length() == 2) {return "000000" + s;}
    else
    if (s.length() == 3) {return "00000" + s;}
    else
    if (s.length() == 4) {return "0000" + s;}
    else
    if (s.length() == 5) {return "000" + s;}
    else
    if (s.length() == 6) {return "00" + s;}
    else
    if (s.length() == 7) {return "0" + s;}
    else{
        return s;
    }

}//end of Status::toHex8String
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Status::toUnsignedHex8String
//
// Converts an unsigned integer to an 8 character hex string.
//
// Since Java does not implement unsigned variables, the unsigned integer is
// transferred as a long value which is large enough to contain the full
// positive value of an unsigned integer
//

static String toUnsignedHex8String(long pValue)
{

    String s = Long.toString(pValue, 16);

    //force length to be eight characters

    if (s.length() == 0) {return "00000000" + s;}
    else
    if (s.length() == 1) {return "0000000" + s;}
    else
    if (s.length() == 2) {return "000000" + s;}
    else
    if (s.length() == 3) {return "00000" + s;}
    else
    if (s.length() == 4) {return "0000" + s;}
    else
    if (s.length() == 5) {return "000" + s;}
    else
    if (s.length() == 6) {return "00" + s;}
    else
    if (s.length() == 7) {return "0" + s;}
    else{
        return s;
    }

}//end of Status::toUnsignedHex8String
//-----------------------------------------------------------------------------

}//end of class Status
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
