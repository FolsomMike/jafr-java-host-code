/******************************************************************************
* Title: RobotLink.java
* Author: Mike Schoonover
* Date: 10/18/2017
*
* Purpose:
*
* This class handles the link to the physical robot. It opens and manages an
* Ethernet socket for that purpose.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package robot;

//-----------------------------------------------------------------------------

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import view.ThreadSafeLogger;

//-----------------------------------------------------------------------------
// class RobotLink
//
//
//

public class RobotLink {


   public InetAddress ipAddr;
   String ipAddrS;

    Socket socket = null;
    PrintWriter out = null;
    BufferedReader in = null;
    byte[] inBuffer;
    byte[] outBuffer = new byte[1024];
    DataOutputStream byteOut = null;
    DataInputStream byteIn = null;


   boolean simulate = false;

   ThreadSafeLogger tsLog;

// Commands to mimic buttons on Makeblock IR remote.

public static final int IR_BUTTON_POWER    = 0x45;
public static final int IR_BUTTON_A        = 0x45;
public static final int IR_BUTTON_B        = 0x46;
public static final int IR_BUTTON_MENU     = 0x47;
public static final int IR_BUTTON_C        = 0x47;
public static final int IR_BUTTON_TEST     = 0x44;
public static final int IR_BUTTON_D        = 0x44;
public static final int IR_BUTTON_PLUS     = 0x40;
public static final int IR_BUTTON_UP       = 0x40;
public static final int IR_BUTTON_RETURN   = 0x43;
public static final int IR_BUTTON_E        = 0x43;
public static final int IR_BUTTON_PREVIOUS = 0x07;
public static final int IR_BUTTON_LEFT     = 0x07;
public static final int IR_BUTTON_PLAY     = 0x15;
public static final int IR_BUTTON_SETTING  = 0x15;
public static final int IR_BUTTON_NEXT     = 0x09;
public static final int IR_BUTTON_RIGHT    = 0x09;
public static final int IR_BUTTON_MINUS    = 0x19;
public static final int IR_BUTTON_DOWN     = 0x19;
public static final int IR_BUTTON_CLR      = 0x0D;
public static final int IR_BUTTON_F        = 0x0D;
public static final int IR_BUTTON_0        = 0x16;
public static final int IR_BUTTON_1        = 0x0C;
public static final int IR_BUTTON_2        = 0x18;
public static final int IR_BUTTON_3        = 0x5E;
public static final int IR_BUTTON_4        = 0x08;
public static final int IR_BUTTON_5        = 0x1C;
public static final int IR_BUTTON_6        = 0x5A;
public static final int IR_BUTTON_7        = 0x42;
public static final int IR_BUTTON_8        = 0x52;
public static final int IR_BUTTON_9        = 0x4A;

static final byte REQUEST_DATA_CMD                  = 0x00;
static final byte SET_HEAD_ANGLE_CMD                = 0x01;
static final byte SET_LED_DIGIT_DISPLAY_CMD         = 0x02;
static final byte SET_LED_DIGIT_DISPLAY_DIRECT_CMD  = 0x03;
static final byte SET_DISPLAY_STATUS_FLAGS_CMD      = 0x04;

//-----------------------------------------------------------------------------
// RobotLink::RobotLink (constructor)
//

public RobotLink()
{

}//end of RobotLink::RobotLink (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init(ThreadSafeLogger pTSLog)
{

    tsLog = pTSLog;

}// end of RobotLink::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::connect
//
// Establishes the TCP/IP connection to the robot.
//

public void connect(String pIPAddrS)
{

    ipAddrS = pIPAddrS;

    if (ipAddrS == null){
        tsLog.logMessage("Error: IP Address is not set.\n");
        return;
    }

    try{
        ipAddr = InetAddress.getByName(ipAddrS);
    }catch(UnknownHostException e) {
        // TODO Auto-generated catch block

    }

    //see notes above regarding IP Addresses

    try {

        //displays message on bottom panel of IDE
        tsLog.logMessage("Connecting to robot...\n");

        if (!simulate) {socket = new Socket(ipAddr, 5000);}
        else {
            /*
            UTSimulator utSimulator = new UTSimulator(
                     ipAddr, 23, mainFileFormat, simulationDataSourceFilePath);
            utSimulator.init();

            socket = utSimulator;
            */
        }

        //set amount of time in milliseconds that a read from the socket will
        //wait for data - this prevents program lock up when no data is ready
        socket.setSoTimeout(250);

        // the buffer size is not changed here as the default ends up being
        // large enough - use this code if it needs to be increased
        //socket.setReceiveBufferSize(10240 or as needed);

        //allow verification that the hinted size is actually used
        tsLog.logMessage(ipAddrS + " receive buffer size: " +
                                      socket.getReceiveBufferSize() + "...\n");

        //allow verification that the hinted size is actually used
        tsLog.logMessage(ipAddrS + " send buffer size: " +
                                        socket.getSendBufferSize() + "...\n");

        out = new PrintWriter(socket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(
                                            socket.getInputStream()));

        byteOut = new DataOutputStream(socket.getOutputStream());
        byteIn = new DataInputStream(socket.getInputStream());

    }
    catch (UnknownHostException e) {
        //logSevere(e.getMessage() + " - Error: 732");
        tsLog.logMessage("Unknown host: UT " + ipAddrS + ".\n");
        return;
    }
    catch (IOException e) {
        //logSevere(e.getMessage() + " - Error: 737");
        tsLog.logMessage("Couldn't get I/O for UT " + ipAddrS + "\n");
        tsLog.logMessage("--" + e.getMessage() + "--\n");
        return;
    }

    try {
        //display the greeting message sent by the remote
        tsLog.logMessage(
                    "\nRobot " + ipAddrS + " sez: " + in.readLine() + "\n");
    }
    catch(IOException e){
        tsLog.logMessage(e.getMessage() + " - Error: 206");
    }

}// end of RobotLink::connect
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::sendInitCommands
//

public void sendInitCommands(){



}// end of RobotLink::sendInitCommands
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::setDisplayStatusFlags
//
// Sends command to the robot to set its Status Display Flags byte. That byte
// controls what status information is displayed on the robot's LED Digit
// Display.
//

public void setDisplayStatusFlags(byte pStatusDisplayFlags){

    sendPacket((byte)SET_DISPLAY_STATUS_FLAGS_CMD,
                pStatusDisplayFlags, (byte)0, (byte)0, (byte)0);

}// end of RobotLink::setDisplayStatusFlags
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::setHeadAngle
//
// Sends command to the robot to set its head angular position.
//

public void setHeadAngle(byte pAngle){

    sendPacket((byte)SET_HEAD_ANGLE_CMD, pAngle, (byte)0, (byte)0, (byte)0);

}// end of RobotLink::setHeadAngle
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::setLEDDigitDisplay
//
// Sends command to the robot to set its LED Digit Display to pValue.
//
// 0 >= pValue <= 9999
//

public void setLEDDigitDisplay(int pValue){

    if(pValue < 0 || pValue > 9999){ pValue = 9999; }

    sendPacket((byte)SET_LED_DIGIT_DISPLAY_CMD,
            (byte) ((pValue >> 8) & 0xff),
            (byte) (pValue & 0xff),
            (byte)0, (byte)0);

}// end of RobotLink::setLEDDigitDisplay
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::setLEDDigitDisplayDirect
//
// Sends command to the robot to set its LED Digit Display to pDigit3~1.
//
// 0 >= datum[*] <= 0x0f  (add 0x10 to set the decimal point)
//
// If datum[*] = 255, that digit position will be left blank.
//

public void setLEDDigitDisplayDirect(
                           int pDigit3, int pDigit2, int pDigit1, int pDigit0){

    sendPacket((byte)SET_LED_DIGIT_DISPLAY_DIRECT_CMD,
            (byte)pDigit3, (byte)pDigit2, (byte)pDigit1, (byte)pDigit0);

}// end of RobotLink::setLEDDigitDisplayDirect
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::setHeadAngle
//
// Sends command to the robot to set its LED Digit Display to pValue.
//

public void setHeadAngle(int pValue){

    sendPacket((byte)SET_LED_DIGIT_DISPLAY_CMD,
            (byte)0, (byte)0, (byte)0, (byte)0);

}// end of RobotLink::setHeadAngle
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::sendCommand
//
// Sends a packet with command pCommand and zero value data.
//

public void sendCommand(int pCommand){

    sendPacket((byte)pCommand, (byte)0, (byte)0, (byte)0, (byte)0);

}// end of RobotLink::sendCommand
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::sendDataRequest
//

public void sendDataRequest(){

    sendPacket(REQUEST_DATA_CMD,  (byte)0, (byte)0, (byte)0, (byte)0);

    waitSleep(250);

    read();

}// end of RobotLink::sendDataRequest
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::read
//

void read(){

int RCV_PKT_SIZE = 1;

    if(byteIn == null){ return; }

    byte[] rcvBuf = new byte[RCV_PKT_SIZE];
    for (int i = 0; i<RCV_PKT_SIZE; i++){
        rcvBuf[i] = 0;
    }

    try{
        if (byteIn.available() >= RCV_PKT_SIZE) {

            byteIn.read(rcvBuf, 0, RCV_PKT_SIZE);

            int data = (int)(rcvBuf[0] & 0xff);

            tsLog.logMessage("distance: " + data + '\n');

        }
    }
    catch(IOException e){

    }

}// end of RobotLink::read
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::sendPacket
//
// Calculates a checksum for pBytes and sends with 0xaa55 header prepended
// and checksum appended. The header is not included in the checksum.
//

void sendPacket(byte... pBytes)
{

   int i = 0, j, checksum = 0;

   outBuffer[i++] = (byte)0xaa; outBuffer[i++] = (byte)0x55;

    for(j=0; j<pBytes.length; j++){
        outBuffer[i] = pBytes[j];
        checksum += outBuffer[i];
        i++;
    }

    checksum = (byte)(0x100 - (byte)(checksum & 0xff));

    outBuffer[i++] = (byte)checksum;

    //send packet to remote
    if (byteOut != null) {
        try{
              byteOut.write(outBuffer, 0 /*offset*/, i);
              byteOut.flush();
        }
        catch (IOException e) {

        }
    }

}//end of RobotLink::sendPacket
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::findNetworkInterface
//
// Finds the network interface for communication with the remotes. Returns
// null if no suitable interface found.
//
// The first interface which is connected and has an IP address beginning with
// 169.254.*.* is returned.
//
// NOTE: If more than one interface is connected and has a 169.254.*.*
// IP address, the first one in the list will be returned. Will need to add
// code to further differentiate the interfaces if such a set up is to be
// used. Internet connections will typically not have such an IP address, so
// a second interface connected to the Internet will not cause a problem with
// the existing code.
//
// If a network interface is not specified for the connection, Java will
// choose the first one it finds. The TCP/IP protocol seems to work even if
// the wrong interface is chosen. However, the UDP broadcasts for wake up calls
// will not work unless the socket is bound to the appropriate interface.
//
// If multiple interface adapters are present, enabled, and running (such as
// an Internet connection), it can cause the UDP broadcasts to fail.
//

NetworkInterface findNetworkInterface()
{

    tsLog.logMessage("");

    NetworkInterface iFace = null;

    try{
        tsLog.logMessage("Full list of Network Interfaces:" + "\n");
        for (Enumeration<NetworkInterface> en =
              NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {

            NetworkInterface intf = en.nextElement();
            tsLog.logMessage("    " + intf.getName() + " " +
                                                intf.getDisplayName() + "\n");

            for (Enumeration<InetAddress> enumIpAddr =
                     intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {

                String ipAddrStr = enumIpAddr.nextElement().toString();

                tsLog.logMessage("        " + ipAddrStr + "\n");

                if(ipAddrStr.startsWith("/169.254.163.30")){
                    iFace = intf;
                    tsLog.logMessage("==>> Binding to above adapter...\n");
                }
            }
        }
    }
    catch (SocketException e) {
        tsLog.logMessage(" (error retrieving network interface list)" + "\n");
    }

    tsLog.logMessage("\n");

    return(iFace);

}//end of RobotLink::findNetworkInterface
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RobotLink::waitSleep
//
// Sleeps for pTime milliseconds.
//

public void waitSleep(int pTime)
{

    try {Thread.sleep(pTime);} catch (InterruptedException e) { }

}//end of RobotLink::waitSleep
//-----------------------------------------------------------------------------

}//end of class RobotLink
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
